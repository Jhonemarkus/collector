import { GraphQLSchema, GraphQLObjectType } from 'graphql'
import Debug from 'debug'
import { InvalidParamsError } from '../util/errors'
// Mutations
import editCollection from './mutations/edit-collection.mutation'
import editCollectable from './mutations/edit-collectable.mutation'
// Queries
import listCollection from './queries/list-collection.query'
import getCollection from './queries/get-collection.query'
import getCollectable from './queries/get-collectable.query'

const debug = Debug('collector:graphql')

// Construct a schema, using GraphQL schema language
export const schema = new GraphQLSchema({
  query: new GraphQLObjectType({
    name: 'Query',
    fields: {
      listCollection,
      getCollection,
      getCollectable
    }
  }),
  mutation: new GraphQLObjectType({
    name: 'Mutation',
    fields: {
      editCollection,
      editCollectable
    }
  })
})

export const customFormatErrorFn = (error) => {
  if (!error.originalError) {
    return error
  }
  switch (error.originalError.constructor) {
    case InvalidParamsError:
      debug('Invalid Parameter on "%s" at %o', error.path, error.locations)
      return {
        message: 'Invalid params',
        code: 1,
        locations: error.locations,
        path: error.path
      }
    default:
      return error
  }
}