import React, {Component } from 'react'
import Pagination from 'react-bootstrap/Pagination';

const addNumOrNext = (arr, num, max, previous = false) => {
  const factor = previous ? -1 : 1
  if (arr != null && num != null) {
    if (num === 0 || num >= max || arr.find((el) => el === num)) {
      if ((num === 0 && previous) || (num >= max && !previous)) {
        return addNumOrNext(arr, num - factor, max, !previous)
      }
      return addNumOrNext(arr, num + factor, max, previous)
    }
    return arr.push(num)
  }
}

class Paginator extends Component {
  constructor(props) {
    super(props)
    this.state =  {
      pageSize: props.pageSize | 10
    }
  }

  fillPages(pageCount) {
    const {page, onChange} = this.props
    // start pages with the first
    const pageNumList = [1]
    if (pageCount <= 5) {
      // Display all pages
      for (let i = 2; i <= pageCount; i++) {
        pageNumList.push(i)
      }
    } else {
      // add one page before the active
      addNumOrNext(pageNumList, page - 1, pageCount)
      // add the active page
      addNumOrNext(pageNumList, page, pageCount)
      // add one page after the active
      addNumOrNext(pageNumList, page + 1, pageCount, true)
      // add last page
      pageNumList.push(pageCount)
    }
    pageNumList.sort((one,two) => one > two)
    const pages = []
    pageNumList.forEach((num, index, list) => {
      if (index > 0 && list[index-1] !== num -1) {
        // Add ellipsis
        pages.push(<Pagination.Ellipsis key={`ellipsis-${index}`}></Pagination.Ellipsis>)
      }
      if (page === num)  {
        pages.push(<Pagination.Item active key={num}>{num}</Pagination.Item>)
      } else {
        pages.push(<Pagination.Item key={num} onClick={() => onChange(num)}>{num}</Pagination.Item>)
      }
    })
    return pages
  }

  generatePages() {
    const {page, total, onChange } = this.props
    const {pageSize} = this.state
    const pages = [<Pagination.Prev key="prev" onClick={() => onChange(page-1)}/>]
    if (total) {
      const pageCount = Math.ceil(total / pageSize)
      pages.push(this.fillPages(pageCount))
    }
    pages.push(<Pagination.Next key="next" onClick={() => onChange(page+1)}/>)
    return pages
  }

  render() {
    const pages = this.generatePages()
    return (
      <div className="center-container" data-testid="paginator">
        <div className="center-wrapper">
          <Pagination>
            {pages}
          </Pagination>
        </div>
      </div>
    )
  }
}

export default Paginator
