import React from 'react'
import { cleanup, fireEvent } from 'react-testing-library'
import { fullRender } from '../TestUtils'
import CollectableField from '../../components/collectable-field'

describe('CollectableField', () => {
  afterEach(cleanup)

  test('have name/type/user filled', () => {
    const onChange = () => {}
    const isInvalid = false
    const { getByLabelText } = fullRender(<CollectableField onChange={onChange} isInvalid={isInvalid} />)
    getByLabelText('Name')
    getByLabelText('User Filled?')
    getByLabelText('Type')
  })

  test('show value list when type === "LIST"', () => {
    const onChange = () => {}
    const isInvalid = false
    const { getByLabelText, queryByLabelText } = fullRender(<CollectableField onChange={onChange} isInvalid={isInvalid} />)
    getByLabelText('Name')
    getByLabelText('User Filled?')
    const type = getByLabelText('Type')
    const valueList = queryByLabelText('Value List')
    expect(valueList).toBeNull()
    // After changing type it should show the value list
    fireEvent.change(type, {target: {value: 'LIST'}})
    getByLabelText('Value List')
  })

  test('display value list as badges', () => {
    const onChange = () => {}
    const isInvalid = false
    const { getByLabelText, getByText } = fullRender(<CollectableField onChange={onChange} isInvalid={isInvalid} />)
    
    const type = getByLabelText('Type')
    fireEvent.change(type, {target: {value: 'LIST'}})

    const valueList = getByLabelText('Value List')
    const value = 'Some Value'
    fireEvent.change(valueList, {target: {value}})
    fireEvent.keyDown(valueList, {key: 'Enter', code: 13})

    const badge = getByText(value)
    expect(badge).toBeInstanceOf(HTMLSpanElement)
    expect(badge.className).toBe('badge badge-dark')
  })

  test('add a badge with the field name and type', () => {
    const onChange = () => {}
    const isInvalid = false
    const { getByLabelText, getByTestId, getByText } = fullRender(<CollectableField onChange={onChange} isInvalid={isInvalid} />)

    const name = getByLabelText('Name')
    fireEvent.change(name, {target: {value: 'New Field'}})

    const type = getByLabelText('Type')
    fireEvent.change(type, {target: {value: 'STRING'}})

    const addBtn = getByTestId('add-button')
    fireEvent.click(addBtn)

    getByText('New Field | STRING')
  })

  test('display fields from props', () => {
    const fields = [{
      name: 'Name',
      type: 'STRING',
      userFilled: false,
      valueList: []
    }]
    const { getByText } = fullRender(<CollectableField onChange={jest.fn()} isInvalid={false} userFields={fields} />)
    getByText('Name | STRING')
  })

  test('remove a value', () => {
    const fields = [{
      name: 'Name',
      type: 'STRING',
      userFilled: false,
      valueList: []
    }, {
      name: 'Size',
      type: 'NUMBER',
      userFilled: false,
      valueList: []
    }]
    const { getByText, queryByText } = fullRender(<CollectableField onChange={jest.fn()} isInvalid={false} userFields={fields} />)
    getByText('Name | STRING')
    const sizeBadge = getByText('Size | NUMBER')
    fireEvent.click(sizeBadge)
    const removeBtn = getByText('Remove')
    fireEvent.click(removeBtn)
    expect(queryByText('Size | NUMBER')).toBeNull()
    getByText('Name | STRING')
  })

  test('update field', () => {
    const fields = [{
      name: 'Name',
      type: 'STRING',
      userFilled: false,
      valueList: []
    }]
    const { getByText, getByLabelText, queryByText } = fullRender(<CollectableField onChange={jest.fn()} isInvalid={false} userFields={fields} />)
    const badge = getByText('Name | STRING')
    fireEvent.click(badge)
    const name = getByLabelText('Name')
    fireEvent.change(name, { target: { value: 'Label' }})
    const updateBtn = getByText('Update')
    fireEvent.click(updateBtn)
    getByText('Label | STRING')
    expect(queryByText('Name | STRING')).toBeNull()
  })
})
