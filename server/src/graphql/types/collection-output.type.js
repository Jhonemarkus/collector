import { GraphQLInt, GraphQLNonNull, GraphQLList, GraphQLObjectType } from 'graphql'
import Collection from './collection.type'

const CollectionOutput = new GraphQLObjectType({
  name: 'CollectionOutput',
  fields: {
    count: { type: GraphQLInt},
    list: { type: new GraphQLNonNull(new GraphQLList(Collection))}
  }
})

export default CollectionOutput
