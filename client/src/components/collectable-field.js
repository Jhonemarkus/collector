import React, { Component } from 'react'
import { connect } from 'react-redux'
import Card from 'react-bootstrap/Card'
import Button from 'react-bootstrap/Button'
import Badge from 'react-bootstrap/Badge'
import FormLabel from 'react-bootstrap/FormLabel'
import FormGroup from 'react-bootstrap/FormGroup'
import FormControl from 'react-bootstrap/FormControl'
import FormCheck from 'react-bootstrap/FormCheck'
import Feedback from 'react-bootstrap/Feedback'

const defaultFormState = {
  name: '',
  type: 'NUMBER',
  valueList: [],
  userFilled: false,
  rawValueList: ''
}

class CollectableField extends Component {
  constructor(props){
    super(props)
    this.state = {
      fieldList: props.userFields || [],
      selectedIndex: null,
      ...defaultFormState,
      validation: {
        name: null,
        valueList: null,
      }
    }
  }

  inputChange = (event) => {
    this.setState({ [event.target.name]: event.target.value })
  }

  checkboxChange = (event) => {
    this.setState({ [event.target.name]: event.target.checked })
  }

  addField = () => {
    if (!this.isFormValid()) {
      return
    }
    const { name, type, valueList, userFilled, fieldList, selectedIndex } = this.state
    const index = isNaN(selectedIndex) ? null : selectedIndex
    const exists = index === null && fieldList.find((field) => field.name === name)
    if (exists) {
      console.log('duplicate field')
    } else {
      const newFieldList = [...fieldList]
      if (index === null){
        newFieldList.push({name, type, userFilled, valueList})
      } else {
        newFieldList[index] = {name, type, userFilled, valueList}
      }
      this.setState({
        fieldList: newFieldList,
        selectedIndex: null,
        ...defaultFormState
      })
      if (this.props.onChange) {
        this.props.onChange(newFieldList)
      }
    }
  }

  selectField = (index) => {
    let field = {...defaultFormState}
    if (index !== null && index >= 0 && index < this.state.fieldList.length) {
      field = {...this.state.fieldList[index]}
    }
    this.setState({
      selectedIndex: index,
      ...field
    })
  }

  removeField = () => {
    const {selectedIndex, fieldList} = this.state
    const newFieldList = fieldList.filter((field, index) => selectedIndex !== index)
    this.setState({
      fieldList: newFieldList,
      selectedIndex: null,
      ...defaultFormState
    })
    if (this.props.onChange) {
      this.props.onChange(newFieldList)
    }
  }

  addValue = (event) => {
    if (event.key === 'Enter') {
      event.preventDefault()
      const {rawValueList, valueList} = this.state
      if (rawValueList.trim() !== '') {
        this.setState({
          rawValueList: '',
          valueList: [...valueList, rawValueList]
        })
      }
    }
  }

  isFormValid = () => {
    const { validation, name, type, valueList } = this.state
    Object.keys(validation).forEach(key => validation[key] = false)
    if (name == null || name.trim() === '') {
      validation.name = true
    }
    if (type === 'LIST' && ( valueList == null || valueList.length === 0) ) {
      validation.valueList = true
    }
    this.setState({ validation })
    return !Object.values(validation).some(invalid => invalid)
  }

  renderFields = () => {
    const { fieldList, selectedIndex } = this.state
    return (
      <div>
        {fieldList.map((field,i) => (
          <Badge key={field.name} variant={i === selectedIndex ? 'info' : 'primary'} onClick={() => this.selectField(i)}>
            {field.name} | {field.type}
          </Badge>
        ))}
      </div>
    )
  }

  renderButtons = () => {
    const {selectedIndex} = this.state
    const {addField, selectField, removeField} = this
    if (selectedIndex !== null && selectedIndex >= 0) {
      return (<div>
        <Button variant="success" type="button" onClick={addField}>Update</Button>
        <Button variant="secondary" type="button" onClick={() => selectField(null)}>Cancel</Button>
        <Button variant="danger" type="button" onClick={removeField}>Remove</Button>
      </div>)
    }
    return (<Button variant="success" type="button" onClick={addField} data-testid="add-button">Add</Button>)
  }

  render(){
    const { inputChange, checkboxChange, renderButtons, addValue, props: {isInvalid} } = this
    const { name, type, userFilled, rawValueList, valueList, validation } = this.state

    return (
      <Card className={isInvalid ? 'invalid' : ''}>
        <Card.Body>
          <Card.Title>
            Fields
          </Card.Title>
              <FormGroup>
                {this.renderFields()}
                <FormControl type="hidden" isInvalid={isInvalid} />
                <Feedback type="invalid">At least one field is required</Feedback>
              </FormGroup>
              <FormGroup>
                <FormLabel htmlFor="field-name">Name</FormLabel>
                <FormControl id="field-name" name="name" onChange={inputChange} value={name} isInvalid={validation.name} />
                <Feedback type="invalid">Name is Required</Feedback>
              </FormGroup>

              <FormGroup>
                <FormLabel htmlFor="type">Type</FormLabel>
                <FormControl as="select" id="type" name="type" onChange={inputChange} value={type} >
                  <option value="NUMBER">Number</option>
                  <option value="STRING">Text</option>
                  <option value="LIST">List</option>
                </FormControl>
              </FormGroup>

              { type === 'LIST' &&
                <FormGroup>
                  <FormLabel htmlFor="rawValueList">Value List</FormLabel>
                  <FormControl
                    id="rawValueList"
                    name="rawValueList"
                    value={rawValueList}
                    onChange={inputChange}
                    onKeyDown={addValue}
                    isInvalid={validation.valueList}
                  />
                  {valueList.map((val) => <Badge key={val} variant="dark">{val}</Badge>)}
                  <Feedback type="invalid">At least one value is required</Feedback>
                </FormGroup>
              }
              
              <FormGroup>
                <FormCheck label={<FormCheck.Label htmlFor="userFilled">User Filled?</FormCheck.Label>} id="userFilled" name="userFilled" onChange={checkboxChange} checked={userFilled} />
              </FormGroup>

              {renderButtons()}
        </Card.Body>
      </Card>
    )
  }
}

export default connect()(CollectableField)
