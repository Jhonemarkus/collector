import { GraphQLEnumType } from 'graphql'

const FieldType = new GraphQLEnumType({
  name: 'FieldType',
  values: {
    NUMBER: { value: 'NUMBER' },
    STRING: { value: 'STRING' },
    LIST: { value: 'LIST' }
  }
})

export default FieldType
