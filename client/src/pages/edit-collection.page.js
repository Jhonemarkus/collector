import React, {Component} from 'react'
import { connect } from 'react-redux'
import { Redirect } from 'react-router-dom'
import { getCollection, doneRedirect } from '../store/actions'
import Loading from '../components/loading'
import CollectionForm from '../components/collection-form'

class EditCollectionPage extends Component {
  componentDidMount(){
    if (this.props.match.params.id) {
      this.props.getCollection(this.props.match.params.id)
    }
  }

  render() {
    const { current, loading, redirect, doneRedirect } = this.props
    if (redirect) {
      doneRedirect()
      return <Redirect to="/collection" />
    }
    if (loading) {
      return <Loading />
    }
    return <CollectionForm collection={current} />
  }
}

const state2props = state => ({
  current: state.collection.current,
  loading: state.collection.loading,
  redirect: state.collection.redirect
})

export default connect(state2props, ({getCollection, doneRedirect}))(EditCollectionPage)
