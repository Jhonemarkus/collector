import React from 'react'
import { cleanup } from 'react-testing-library'
import { fullRender } from '../TestUtils'
import Loading from '../../components/loading'

describe('Loading', () => {
  afterEach(cleanup)

  it('should have text "Loading..."', () => {
    const { getByText } = fullRender(<Loading />)
    getByText('Loading...')
  })
})
