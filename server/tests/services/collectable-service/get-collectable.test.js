import { describe, it, before, after,  } from 'mocha'
import { expect, assert } from 'chai'
import MongoUnit from 'mongo-unit'
import testData from '../../test-data'
import { InvalidParamsError } from '../../../src/util/errors'
import { getCollectable } from '../../../src/services/collectable-service'

describe('CollectableService.getCollectable', () => {
  beforeEach(() => MongoUnit.initDb(process.env.MONGO_URL, testData))
  afterEach(() => MongoUnit.drop())

  it('get a collectable by id', async () => {
    const car = await getCollectable('5d0112113d60d7d2653c5c94')
    expect(car).not.to.be.null
    expect(car.name).to.equal('350Z')
  })

  it('returns null when not found', async () => {
    const car = await getCollectable('non-existing')
    expect(car).to.be.null
  })
})
