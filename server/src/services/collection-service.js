import Collection from '../models/collection'
import { isNullOrEmptyString, isNonEmptyArray } from '../util/validation-util'
import { InvalidParamsError } from '../util/errors'
import config from 'config'

function checkDuplicateName(value) {
  if(value) {
    const names = value.map(el => el.name)
    return names.some((name, i) => names.indexOf(name) !== i)
  }
  return false
}

export const getCollection = async (id) => {
  const collection = await Collection.findById(id)
  return collection
}

export const countCollection = async () => {
  const count = Collection.countDocuments()
  return count
}

export const listCollection = async (page = 1) => {
  if (page < 1) {
    page = 1
  }
  const list = await Collection.find().sort('name').limit(config.pageSize).skip((page - 1) * config.pageSize)
  return list
}

export const editCollection = async ({id, name, collectableName, userFields} = {}) => {
  // Validate input
  if (isNullOrEmptyString(name) || isNullOrEmptyString(collectableName)) {
    throw new InvalidParamsError()
  }
  let collection = new Collection()
  // If edit request (has id) get from mongo
  if (!isNullOrEmptyString(id)) {
    collection = await Collection.findById(id)
  }
  // Update attributes
  collection.name = name
  collection.collectableName = collectableName
  if (checkDuplicateName(userFields)) {
    throw new InvalidParamsError()
  }
  collection.userFields = userFields
  // Save
  await collection.save()
  return collection
}
