import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Button from 'react-bootstrap/Button'
import { Link } from 'react-router-dom'

class CollectionRow extends Component {
  render() {
    const { id, name, collectableName, userFields } = this.props.collection
    return (
      <tr>
        <td>
          {name}
        </td>
        <td>
          {collectableName}
        </td>
        <td>
          {userFields.length}
        </td>
        <td>
          <Link to={`/collection/${id}`}>
            <Button variant="primary" data-testid="edit-button">Edit</Button>
          </Link>
        </td>
      </tr>
    )
  }
}

CollectionRow.propTypes = {
  collection: PropTypes.object.isRequired,
}

export default CollectionRow
