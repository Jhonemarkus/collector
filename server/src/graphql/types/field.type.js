import { GraphQLObjectType, GraphQLString, GraphQLBoolean, GraphQLList, GraphQLNonNull } from 'graphql'
import FieldType from './field-type.enum'

const Field = new GraphQLObjectType({
  name: 'Field',
  fields: {
    name: { type: new GraphQLNonNull(GraphQLString) },
    userFilled: { type: new GraphQLNonNull(GraphQLBoolean) },
    type: { type: new GraphQLNonNull(FieldType) },
    valueList: { type: new GraphQLList(new GraphQLNonNull(GraphQLString)) }
  }
})

export default Field
