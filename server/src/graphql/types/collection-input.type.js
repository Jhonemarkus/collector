import { GraphQLInputObjectType, GraphQLID, GraphQLString, GraphQLNonNull, GraphQLList } from 'graphql'
import FieldInput from './field-input.type'

const CollectionInput = new GraphQLInputObjectType({
  name: 'CollectionInput',
  fields: {
    id: { type: GraphQLID },
    name: { type: new GraphQLNonNull(GraphQLString) },
    collectableName: { type: new GraphQLNonNull(GraphQLString) },
    userFields: { type: new GraphQLList(new GraphQLNonNull(FieldInput)) },
  }
})

export default CollectionInput
