import { all, call, put, takeEvery, takeLatest } from 'redux-saga/effects'
import actions, {
  saveCollectionSuccess,
  listCollectionSuccess,
  getCollectionSuccess,
  setError
} from './actions'
import { post } from './api'

export function* getCollection(action) {
  try {
    const body = {
      query: `{ getCollection(id: "${action.id}"){
        id name collectableName
        userFields{ name type valueList userFilled }
      } }`
    }
    const response = yield call(post, body)
    yield put(getCollectionSuccess(response.data.getCollection))
  } catch (error) {
    console.error(error)
    yield put(setError('collection',error))
  }
}

export function* saveCollection(action) {
  try {
    const body = {
      query: `
      mutation editCollection($collection: CollectionInput!){
        editCollection (collection: $collection) {
          id
          name
          collectableName
          userFields {
            name
            type
            valueList
            userFilled
          }
        }
      }
      `,
      variables: {
        collection: action.collection
      }
    }
    const response = yield call(post, body)
    if (response.data) {
      yield put(saveCollectionSuccess(response.data.editCollection))
    } else {
      console.error(response)
      yield put(setError('collection', 'An error ocurred when saving the collection'))
    }
  } catch (error) {
    console.error(error)
    yield put(setError('collection', error))
  }
}

export function* listCollection(action) {
  try {
    const body = {
      query: `{ listCollection(page:${action.page}){
        count
        list {
          id name collectableName
          userFields{ name type valueList userFilled }
        }
      } }`
    }
    const response = yield call(post, body)
    yield put(listCollectionSuccess(response.data.listCollection, action.page))
  } catch (err) {
    console.error(err)
    yield put(setError('collection',err))
  }
}

export default function* rootSaga() {
  yield all([
    yield takeLatest(actions.SAVE_COLLECTION, saveCollection),
    yield takeEvery(actions.LIST_COLLECTION, listCollection),
    yield takeLatest(actions.GET_COLLECTION, getCollection)
  ])
}
