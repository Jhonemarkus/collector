import {ObjectId} from 'mongodb'

export default {
  collection: [
    {
      name: 'GT Sport',
      collectableName: 'Car',
      _id: new ObjectId('5cdae0cb000a242ff80ff9e1'),
      userFields: [{
        name: 'Color',
        userFilled: true,
        type: 'STRING'
      }]
    }, {
      name: 'GT Sport 2',
      collectableName: 'Car',
      _id: new ObjectId('5cdae0cb000a242ff80ff9e2'),
      userFields: [{
        name: 'Color',
        userFilled: true,
        type: 'STRING'
      }]
    }, {
      name: 'GT Sport 3',
      collectableName: 'Car',
      _id: new ObjectId('5cdae0cb000a242ff80ff9e3'),
      userFields: [{
        name: 'Color',
        userFilled: true,
        type: 'STRING'
      }]
    }, {
      name: 'GT Sport 4',
      collectableName: 'Car',
      _id: new ObjectId('5cdae0cb000a242ff80ff9e4'),
      userFields: [{
        name: 'Color',
        userFilled: true,
        type: 'STRING'
      }]
    }, {
      name: 'GT Sport 5',
      collectableName: 'Car',
      _id: new ObjectId('5cdae0cb000a242ff80ff9e5'),
      userFields: [{
        name: 'Color',
        userFilled: true,
        type: 'STRING'
      }]
    }, {
      name: 'GT Sport 6',
      collectableName: 'Car',
      _id: new ObjectId('5cdae0cb000a242ff80ff9e6'),
      userFields: [{
        name: 'Color',
        userFilled: true,
        type: 'STRING'
      }]
    }, {
      name: 'GT Sport 7',
      collectableName: 'Car',
      _id: new ObjectId('5cdae0cb000a242ff80ff9e7'),
      userFields: [{
        name: 'Color',
        userFilled: true,
        type: 'STRING'
      }]
    }, {
      name: 'GT Sport 8',
      collectableName: 'Car',
      _id: new ObjectId('5cdae0cb000a242ff80ff9e8'),
      userFields: [{
        name: 'Color',
        userFilled: true,
        type: 'STRING'
      }]
    }, {
      name: 'GT Sport 9',
      collectableName: 'Car',
      _id: new ObjectId('5cdae0cb000a242ff80ff9e9'),
      userFields: [{
        name: 'Color',
        userFilled: true,
        type: 'STRING'
      }]
    }, {
      name: 'GT Sport 10',
      collectableName: 'Car',
      _id: new ObjectId('5cdae0cb000a242ff80ff910'),
      userFields: [{
        name: 'Color',
        userFilled: true,
        type: 'STRING'
      }]
    }, {
      name: 'GT Sport 11',
      collectableName: 'Car',
      _id: new ObjectId('5cdae0cb000a242ff80ff911'),
      userFields: [{
        name: 'Color',
        userFilled: true,
        type: 'STRING'
      }]
    }
  ],
  collectable: [{
    _id: new ObjectId('5d0112113d60d7d2653c5c94'),
    parent: new ObjectId('5cdae0cb000a242ff80ff9e1'),
    name: '350Z',
    fields: [{
      name: 'Color',
      value: 'Gray'
    }]
  }]
}
