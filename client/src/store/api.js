const URL = 'http://localhost:4000/graphql'

export const post = async (body) => {
  const result = await fetch(URL, {
    method: 'POST',
    body: JSON.stringify(body),
    headers: {
      'Content-Type': 'application/json'
    }
  })
  const data = await result.json()
  return data
}