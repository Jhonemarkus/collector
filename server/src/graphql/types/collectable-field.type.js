import { GraphQLObjectType, GraphQLString, GraphQLBoolean, GraphQLList, GraphQLNonNull } from 'graphql'

const CollectableField = new GraphQLObjectType({
  name: 'CollectableField',
  fields: {
    name: { type: new GraphQLNonNull(GraphQLString) },
    value: { type: new GraphQLNonNull(GraphQLString) }
  }
})

export default CollectableField
