import React from 'react'
import { cleanup } from 'react-testing-library'
import { fullRender } from './TestUtils'
import AppContainer from '../AppContainer'

describe('AppContainer', () => {
  afterEach(cleanup)

  test('navbar and container', () => {
    const { container, getByText } = fullRender(<AppContainer><span>Body</span></AppContainer>)
    const navbar = container.querySelector('nav.navbar')
    expect(navbar).toBeTruthy()
    const bodyContainer = container.querySelector('div.container-fluid')
    expect(bodyContainer).toBeTruthy()
    getByText('Body')
  })

  test('display errors', () => {
    const error = 'Something wrong is not right'
    const {getByTestId, getByText} = fullRender(<AppContainer><span>Body</span></AppContainer>, {
      initialState: {error}
    })
    getByTestId('error-display')
    getByText(error)
  })
})
