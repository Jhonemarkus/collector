import React, { Component, Fragment } from 'react'
import { connect } from 'react-redux'
import Form from 'react-bootstrap/Form'
import FormGroup from 'react-bootstrap/FormGroup'
import FormLabel from 'react-bootstrap/FormLabel'
import FormControl from 'react-bootstrap/FormControl'
import Feedback from 'react-bootstrap/Feedback'
import Button from 'react-bootstrap/Button'
import { Redirect } from 'react-router-dom'

import { saveCollection, doneRedirect, getCollection } from '../store/actions'
import Field from './collectable-field'
import Loading from './loading'

class CollectionForm extends Component {
  constructor(props) {
    super(props)
    const collection = props.collection || {name: '', collectableName: '', userFields: ''}
    this.state = {
      collection,
      validation: {
        name: null,
        collectableName: null,
        userFields: null
      }
    }
  }

  componentDidMount() {
    if (this.props.match && this.props.match.params.id) {
      this.props.getCollection(this.props.match.params.id)
    }
  }

  onChangeFieldList = (userFields) => {
    const { collection } = this.state
    this.setState({collection: {...collection, userFields } })
  }

  onChangeInput = ({target: {name,value}}) => {
    const { collection } = this.state
    this.setState({collection: { ...collection, [name]: value } })
  }

  isValid = () => {
    const { validation, collection: {name, collectableName, userFields} } = this.state
    Object.keys(validation).forEach(key => validation[key] = false)
    if (!name || name.trim() === '') {
      validation.name = true
    }
    if (!collectableName || collectableName.trim() === '') {
      validation.collectableName = true
    }
    if (! Array.isArray(userFields) || userFields.length === 0) {
      validation.userFields = true
    }
    this.setState({validation})
    return !Object.values(validation).some(el => el)
  }

  onSubmit = (event) => {
    event.preventDefault()
    if (this.isValid()) {
      this.props.saveCollection(this.state.collection)
    }
  }

  render() {
    const { loading, redirect } = this.props
    if (loading) {
      return <Loading />
    }
    if (redirect) {
      this.props.doneRedirect()
      return <Redirect to="/collection" />
    }
    const { collection: { name, collectableName, userFields }, validation } = this.state
    const title = name !== '' ? name : 'New Collection'
    return (
      <Fragment>
        <h2>{title}</h2>
        <Form onSubmit={this.onSubmit}>
          <FormGroup>
            <FormLabel htmlFor="name">Name</FormLabel>
            <FormControl id="name" name="name" onChange={this.onChangeInput} value={name} isInvalid={validation.name}/>
            <Feedback type="invalid">Name is Required</Feedback>
          </FormGroup>

          <FormGroup>
            <FormLabel htmlFor="collectableName">Collectable Name</FormLabel>
            <FormControl
              id="collectableName"
              name="collectableName"
              onChange={this.onChangeInput}
              value={collectableName}
              isInvalid={validation.collectableName}
              />
            <Feedback type="invalid">Collectable Name is required</Feedback>
          </FormGroup>

          <FormGroup>
            <Field onChange={this.onChangeFieldList} isInvalid={validation.userFields} userFields={userFields}/>
          </FormGroup>
          
          <Button variant="primary" type="submit" data-testid="submitBtn">Save</Button>
        </Form>
    </Fragment>
    )
  }
}

const state2props = state => ({
  loading: state.collection.loading,
  current: state.collection.current,
  redirect: state.redirect,
})

export default connect(state2props, { saveCollection, doneRedirect, getCollection })(CollectionForm)
