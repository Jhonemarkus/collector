import Collectable from '../models/collectable'
import { isNullOrEmptyString, isNonEmptyArray } from '../util/validation-util'
import { InvalidParamsError } from '../util/errors'
import config from 'config'

export const getCollectable = async (id) => {
  return await Collectable.findById(id)
}

export const editCollectable = async ({id, parent, name, fields}) => {
  // Validate
  if (!name || (!id && !parent)) {
    throw new InvalidParamsError()
  } 
  // Load or create new
  let collectable = new Collectable()
  collectable.parent = parent
  if (id) {
    collectable = await Collectable.findById(id)
    if (!collectable) {
      throw new InvalidParamsError()
    }
  }
  collectable.name = name
  collectable.fields = fields
  await collectable.save()
  return collectable
}
