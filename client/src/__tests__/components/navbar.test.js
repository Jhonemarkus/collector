import React from 'react'
import { cleanup } from 'react-testing-library'
import { fullRender } from '../TestUtils'
import Navbar from '../../components/navbar'

describe('Navbar', () => {
  afterEach(cleanup)

  it('should have links', () => {
    const { getByText } = fullRender(<Navbar />)
    const home = getByText('Home')
    expect(home).toBeInstanceOf(HTMLAnchorElement)
    expect(home.href).toBe('http://localhost/')
    
    const collection = getByText('Collection')
    expect(collection).toBeInstanceOf(HTMLAnchorElement)
    expect(collection.href).toBe('http://localhost/collection')
  })
})
