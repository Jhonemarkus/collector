import { GraphQLObjectType, GraphQLString, GraphQLList, GraphQLID, GraphQLNonNull } from 'graphql'
import Field from './collectable-field.type'

const Collectable = new GraphQLObjectType({
  name: 'Collectable',
  fields: {
    id: { type: GraphQLID },
    name: { type: new GraphQLNonNull(GraphQLString) },
    collection: { type: new GraphQLNonNull(GraphQLID) },
    fields: { type: new GraphQLList(new GraphQLNonNull(Field)) }
  }
})

export default Collectable
