import config from 'config'
import express from 'express'
import expressGraphQL from 'express-graphql'
import cors from 'cors'

import setupConnection from './mongoose'
import { schema, customFormatErrorFn } from './graphql'

// *** MongoDB ***
setupConnection()

// *** Express ***
const app = express();

// CORS
app.use(cors())

// GraphQL
app.use('/graphql', expressGraphQL({
  schema,
  //rootValue,
  graphiql: config.get('graphiql'),
  customFormatErrorFn
}));

const listen = app.listen(config.get('port'), () => {
  console.log(`Server is running on port ${config.get('port')} and in ${config.get('name')} mode`);
});

module.exports = app;
module.exports.port = listen.address().port;
