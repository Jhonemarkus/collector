import mongoose from 'mongoose'
import config from 'config'
import Debug from 'debug'

const debug = Debug('collector:mongoose')

const url = process.env.MONGO_URL || config.mongodb.url
debug(`MongoUnit url: ${process.env.MONGO_URL}`)

export default function setupConnection() {
  mongoose.set('useCreateIndex', true);
  const promise = mongoose.connect(url, config.mongodb.options)
  mongoose.connection.on('open', debug.bind(debug, 'Connected to MongoDB'))
  mongoose.connection.on('error', debug.bind(debug, 'connection error:'))
  return promise
}
