import React from 'react'
import { render, cleanup, fireEvent } from 'react-testing-library'
import Paginator from '../../components/paginator'

describe('Paginator', () => {
  afterEach(cleanup)

  test('should call onChange on link click', () => {
    const onChange = jest.fn()
    const { getByText } = render(<Paginator page={2} total={30} onChange={onChange} />)
    const prev = getByText('Previous')
    const next = getByText('Next')
    const one = getByText('1')
    expect(onChange).not.toHaveBeenCalled()
    //Click on link '1'
    fireEvent.click(one)
    expect(onChange).toHaveBeenCalledTimes(1)
    expect(onChange).lastCalledWith(1)
    // Click on 'next'
    fireEvent.click(next)
    expect(onChange).toHaveBeenCalledTimes(2)
    expect(onChange).lastCalledWith(3)
    // Click on 'prev'
    fireEvent.click(prev)
    expect(onChange).toHaveBeenCalledTimes(3)
    expect(onChange).lastCalledWith(1)
  })

  test('shouldn\'t call onChange for active link', () => {
    const onChange = jest.fn()
    const { getByText } = render(<Paginator page={1} total={20} />)
    const one = getByText('1')
    fireEvent.click(one)
    expect(onChange).not.toHaveBeenCalled()
  })

  test('Should display only previous and next', () => {
    const { getByText, queryByText } = render(<Paginator />)
    getByText('Previous')
    getByText('Next')
    const pages = queryByText('1')
    expect(pages).toBeNull()
  })

  test('Should display the current page', () => {
    const { getByText, container } = render(<Paginator total={11} page={2} />)
    const active = container.querySelectorAll('.active')
    getByText('1')
    getByText('2')
    expect(active).toHaveLength(1)
    expect(active[0].textContent).toContain('2')
  })

  test('Should display up to 5 pages', () => {
    const { getByText, queryByText } = render(<Paginator total={100} page={1} />)
    getByText('1')
    getByText('2')
    getByText('3')
    getByText('4')
    getByText('10')
    const five = queryByText('5')
    expect(five).toBeNull()
  })

  test('Should display ellipsis after 3 page', () => {
    const { getByText } = render(<Paginator total={100} page={1} />)
    getByText('…')
  })

  test('Should display ellipsis if more than 2 next pages', () => {
    const { getByText } = render(<Paginator total={100} page={10} />)
    getByText('…')
  })

  test('Should display 2 ellipsis', () => {
    const { queryAllByText } = render(<Paginator total={100} page={5} />)
    const ellipsis = queryAllByText('…')
    expect(ellipsis).toHaveLength(2)
  })
})