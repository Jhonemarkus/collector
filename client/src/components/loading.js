import React from 'react'
import ProgressBar from 'react-bootstrap/ProgressBar'

const Loading = props => (
  <ProgressBar className="loading" striped animated now={100} variant="info" label="Loading..." />
)

export default Loading
