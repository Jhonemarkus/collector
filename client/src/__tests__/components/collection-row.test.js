import React from 'react'
import { cleanup, fireEvent } from 'react-testing-library'
import { fullRender, throwOnPropTypeError } from '../TestUtils'
import CollectionRow from '../../components/collection-row'

const collection = {
  id: 'abc',
  name: 'GT Sports Cars',
  collectableName: 'Car',
  userFields: [{
    name: 'Name',
    type: 'STRING',
    userFilled: false
  }]
}

describe('CollectionRow', () => {
  beforeAll(throwOnPropTypeError)

  afterEach(cleanup)
  
  afterAll(() => {
    jest.restoraAllMocks()
  })

  test('should fail if incorrect props', () => {
    expect(() => fullRender(<CollectionRow />)).toThrow()
  })

  test('should display name/collectable name/#fields/edit button', () => {
    const { getByText, getByTestId } = fullRender(<table><tbody><CollectionRow collection={collection} /></tbody></table>)
    getByText(collection.name)
    getByText(collection.collectableName)
    getByText(`${collection.userFields.length}`)
    getByTestId('edit-button')
  })

  test('should route to edit view', () => {
    const { getByTestId, history } = fullRender(<table><tbody><CollectionRow collection={collection} /></tbody></table>)
    const editBtn = getByTestId('edit-button')
    fireEvent.click(editBtn)
    expect(history.entries).toEqual(expect.arrayContaining([expect.objectContaining({pathname: `/collection/${collection.id}`})]))
  })
})
