import { GraphQLInputObjectType, GraphQLNonNull, GraphQLString, GraphQLBoolean, GraphQLList } from 'graphql'
import FieldType from './field-type.enum'

const FieldInput = new GraphQLInputObjectType({
  name: 'FieldInput',
  fields: {
    name: { type: new GraphQLNonNull(GraphQLString)},
    userFilled: { type: new GraphQLNonNull(GraphQLBoolean) },
    type: { type: FieldType },
    valueList: { type: new GraphQLList(new GraphQLNonNull(GraphQLString)) }
  }
})

export default FieldInput
