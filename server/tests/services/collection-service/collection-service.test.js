import { describe, it, before, after,  } from 'mocha'
import { expect, assert } from 'chai'
import MongoUnit from 'mongo-unit'
import { editCollection, listCollection, countCollection } from '../../../src/services/collection-service'
import testData from '../../test-data'
import { InvalidParamsError } from '../../../src/util/errors'
import { getCollection } from '../../../src/services/collection-service'

describe('CollectionService', () => {
  before(() => MongoUnit.initDb(process.env.MONGO_URL, testData))
  after(() => MongoUnit.drop())

  it('get collection by id', async () => {
    const collection = await getCollection(testData.collection[0]._id)
    expect(collection).to.be.an.instanceof(Object)
    expect(collection.name).to.equal(testData.collection[0].name)
  })

  it('count the collections', async () => {
    const count = await countCollection()
    expect(count).to.equal(11)
  })

  it('list collections up to page size', async () => {
    const list = await listCollection()
    expect(list).to.be.an.instanceof(Array)
    expect(list).to.have.length(10)
  })

  it('list second page of collections', async () => {
    const list = await listCollection(2)
    expect(list).to.be.an.instanceof(Array)
    expect(list).to.have.length(1)
  })

  it('add new collection', async () => {
    const params = {
      name: 'Bottle Caps',
      collectableName: 'Cap',
      userFields: [{
        name: 'Maker',
        userFilled: false,
        type: 'LIST',
        valueList: ['Coca-Cola', 'Pepsi']
      }]
    }
    const collection = await editCollection(params)
    expect(collection).not.to.be.null
    expect(collection.id).not.to.be.null
    expect(collection.name).to.equal(params.name)
    expect(collection.collectableName).to.equal(params.collectableName)
    expect(collection.userFields).to.have.length(1)
  })

  it('edit existing collection', async () => {
    const params = {
      id: '5cdae0cb000a242ff80ff9e1',
      name: 'Gran Turismo Sport',
      collectableName: 'Vehicle',
      userFields: []
    }
    const collection = await editCollection(params)
    expect(collection).not.to.be.null
    expect(collection.name).to.equal(params.name)
    expect(collection.collectableName).to.equal(params.collectableName)
    expect(collection.userFields).to.have.length(0)
  })

  it('throw error on invalid parameters', async () => {
    // Null
    try {
      await editCollection()
      assert.fail()
    } catch (e) {
      expect(e).to.be.an.instanceOf(InvalidParamsError)
    }
    // Empty
    try {
      await editCollection({})
      assert.fail()
    } catch (e) {
      expect(e).to.be.an.instanceOf(InvalidParamsError)
    }
    // No name
    try {
      await editCollection({ collectableName: 'My Collectable' })
      assert.fail()
    } catch (e) {
      expect(e).to.be.an.instanceOf(InvalidParamsError)
    }
    // No collectableName
    try {
      await editCollection({ name: 'My Collection' })
      assert.fail()
    } catch (e) {
      expect(e).to.be.an.instanceOf(InvalidParamsError)
    }
  })

  it('not allow two fields with the same name', async () => {
    try {
      await editCollection({
        name: 'My collection',
        collectableName: 'collectable',
        userFields: [{
          name: 'color',
          type: 'STRING',
        }, {
          name: 'color',
          type: 'NUMBER'
        }]
      })
      assert.fail()
    } catch (e) {
      expect(e).to.be.an.instanceOf(InvalidParamsError)
    }
  })
})
