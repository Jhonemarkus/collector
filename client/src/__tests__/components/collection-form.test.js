import React from 'react'
import { cleanup, fireEvent } from 'react-testing-library'
import fetchMock from 'fetch-mock'
import { fullRender } from '../TestUtils'
import CollectionForm from '../../components/collection-form'
import { Route } from 'react-router-dom'
import actions from '../../store/actions';

describe('CollectionForm', () => {
  afterEach(cleanup)

  beforeAll( () => {
    fetchMock.mock('*', {data: { editCollection: {} } } )
  })

  afterAll( () => {
    fetchMock.restore()
  })

  test('have title', () => {
    const { getByText } = fullRender(<CollectionForm />)
    getByText(/new collection/i)
  })
  
  test('don\'t save when invalid', () => {
    const { getByTestId, store } = fullRender(<CollectionForm />)
    const submitBtn = getByTestId('submitBtn')
    fireEvent.click(submitBtn)
    expect(store.dispatch).not.lastCalledWith({type: actions.SAVE_COLLECTION})
  })
  
  test('save collection', async () => {
    const { getByLabelText, getByTestId, getAllByLabelText } = fullRender(<CollectionForm />)
    const name = getAllByLabelText('Name')[0]
    const collectableName = getByLabelText('Collectable Name')
    const fieldName = getAllByLabelText('Name')[1]
    const userFilled = getByLabelText('User Filled?')
    const type = getByLabelText('Type')
    const addBtn = getByTestId('add-button')
    const submitBtn = getByTestId('submitBtn')
    
    const params = {
      collectableName: 'Stamp',
      name: 'New Collection',
      userFields: [{
        name: 'Color',
        userFilled: true,
        type: 'STRING',
        valueList: []
      }]
    }
    
    fireEvent.change(name, { target: { value: params.name } })
    fireEvent.change(collectableName, { target: { value: params.collectableName } })
    fireEvent.change(fieldName, { target: { value: params.userFields[0].name } })
    fireEvent.click(userFilled)
    fireEvent.change(type, { target: { value: params.userFields[0].type } })
    // Add field
    fireEvent.click(addBtn)
    // Submit
    fireEvent.click(submitBtn)
    
    //Check if GraphQL has been called with right values
    await fetchMock.flush(true)
    const call = fetchMock.lastCall()
    const body = JSON.parse(call[1].body)
    expect(body.variables.collection).toEqual(params)
  })
  
  test('change title if in edit mode', () => {
    const collection = { id: 'abc', name: 'My Collection', userFields: []}
    const {getByText} = fullRender(<CollectionForm collection={collection} />)
    getByText(collection.name)
  })

  test('populate form with object values', () => {
    const collection = {
      id: 'someid',
      name: 'Edit test',
      collectableName: 'edit test collectable',
      userFields: [{
        name: 'test name',
        type: 'STRING',
        userFilled: false
      }]
    }
    const {getByLabelText, getAllByLabelText, getByText, store} = fullRender(<CollectionForm collection={collection} />)
    const name = getAllByLabelText('Name')[0]
    const collectableName = getByLabelText('Collectable Name')
    const fieldName = getAllByLabelText('Name')[1]
    
    expect(name.value).toEqual(collection.name)
    expect(collectableName.value).toEqual(collection.collectableName)
    expect(fieldName.value).toEqual('')
    getByText(`${collection.userFields[0].name} | ${collection.userFields[0].type}`)
  })
})
