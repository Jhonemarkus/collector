import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import Container from 'react-bootstrap/Container';

import './App.css'
import Navbar from './components/navbar'
import ErrorDisplay from './components/error-display'

class AppContainer extends Component {
  render() {
    const { children, error } = this.props
    return (
      <Fragment>
        <Navbar />
        <Container fluid>
          <ErrorDisplay error={error}/>
          {children}
        </Container>
      </Fragment>
    )
  }
}

AppContainer.propTypes = {
  children: PropTypes.object.isRequired
}

export default connect(state => ({ error: state.error }))(AppContainer)
