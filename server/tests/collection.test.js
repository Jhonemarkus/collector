import server from '../src/index'
import request from 'supertest'
import { expect } from 'chai'
import testData from './test-data'
import MongoUnit from 'mongo-unit'
import config from 'config'

describe('GraphQL Collections', () => {
  beforeEach(() => MongoUnit.initDb(process.env.MONGO_URL, testData))
  afterEach(() => MongoUnit.drop())

  it('get collection by id', async () => {
    const id = '5cdae0cb000a242ff80ff9e1'
    const response = await request(server).post('/graphql')
      .send({
        query: `{ getCollection(id: "${id}"){
          id name collectableName 
          userFields { name type userFilled valueList }
        } }`
      })
      .expect(200)
      .expect('Content-Type', /json/)
    expect(response.body.data.getCollection).to.be.an.instanceof(Object)
    const collection = response.body.data.getCollection
    expect(collection.id).to.equal(id)
    expect(collection.name).to.equal('GT Sport')
    expect(collection.collectableName).to.equal('Car')
    expect(collection.userFields).to.be.an.instanceof(Array)
    expect(collection.userFields[0].name).to.equal('Color')
    expect(collection.userFields[0].userFilled).to.equal(true)
    expect(collection.userFields[0].type).to.equal('STRING')
  })

  it('save using editCollection', async () => {
    const collection = {
      id: null,
      name: 'Test collection',
      collectableName: 'Test collectable',
      userFields: [{
        name: 'Name',
        type: 'STRING',
        valueList: [],
        userFilled: false
      }]
    }
    const response = await request(server).post('/graphql')
      .send({
        query: `mutation editCollection($collection: CollectionInput!){
          editCollection (collection: $collection) {
            id
            name
            collectableName
            userFields {
              name
              type
              valueList
              userFilled
            }
          }
        }`,
        variables: {
          collection: collection
        }
      })
      .expect(200)
      .expect('Content-Type', /json/)
    // Response OK
    expect(response.body).to.be.an.instanceof(Object)
    expect(response.body.data).to.be.an.instanceof(Object)
    expect(response.body.data.editCollection).to.be.an.instanceof(Object)
    // Fields
    const returned = response.body.data.editCollection
    expect(returned.id).not.to.be.null
    expect(returned.name).to.equal(collection.name)
    expect(returned.collectableName).to.equal(collection.collectableName)
    expect(returned.userFields).to.be.an.instanceof(Array)
    expect(returned.userFields[0].name).to.equal(collection.userFields[0].name)
    expect(returned.userFields[0].type).to.equal(collection.userFields[0].type)
    expect(returned.userFields[0].valueList).to.deep.equal(collection.userFields[0].valueList)
    expect(returned.userFields[0].userFilled).to.equal(collection.userFields[0].userFilled)
  })

  it('listCollection', async () => {
    const response = await request(server).post('/graphql')
      .send({ query: '{ listCollection { count list { name } } }' })
      .expect(200)
      .expect('Content-Type', /json/)
    // Response OK
    expect(response.body).to.be.an.instanceof(Object)
    expect(response.body.data).to.be.an.instanceof(Object)
    expect(response.body.data.listCollection).to.be.an.instanceof(Object)
    const listCollection = response.body.data.listCollection
    expect(listCollection.count).to.equal(11)
    expect(listCollection.list).to.be.an.instanceof(Array)
    // Page size
    expect(listCollection.list.length).to.equal(config.pageSize)
  })
})
