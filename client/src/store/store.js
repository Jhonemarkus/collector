import { createStore, applyMiddleware, compose } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import createSagaMiddleware from 'redux-saga'
import persistState from 'redux-localstorage'

import reducer from './reducer'
import rootSaga from './sagas'

let composeFn = compose
// Check environment
if (true) {
  composeFn = composeWithDevTools
}

const state = {
  collection: {
    list: null,
    current: null,
    loading: false,
    page: 0,
    count: null
  },
  redirect: false,
}

export function setupStore(initialState = state) {
  const sagaMiddleware = createSagaMiddleware()

  const store = createStore(
    reducer,
    initialState,
    composeFn(
      applyMiddleware(sagaMiddleware),
      persistState('persist', {key: 'collector'})
    )
  )
  sagaMiddleware.run(rootSaga)

  return store
}
