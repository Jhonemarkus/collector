import 'bootstrap/dist/css/bootstrap.css'
import React from 'react'
import ReactDOM from 'react-dom'
import * as serviceWorker from './serviceWorker'
import { setupStore } from './store/store'
import { Provider } from 'react-redux'
import CustomRouter from './routes'

const store = setupStore()

function render() {
  ReactDOM.render(
    <Provider store={store}>
      <CustomRouter />
    </Provider>,
    document.getElementById('root'))
}

render()

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister()
