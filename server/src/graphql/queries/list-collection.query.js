import { GraphQLInt, GraphQLNonNull, GraphQLList } from 'graphql'
import CollectionOutput from '../types/collection-output.type'
import { listCollection, countCollection } from '../../services/collection-service'

export default {
  type: new GraphQLNonNull(CollectionOutput),
  args: {
    page: { type: GraphQLInt}
  },
  resolve: async (source, {page}) => {
    const count =  await countCollection()
    const list = await listCollection(page)
    return { count, list }
  }
}
