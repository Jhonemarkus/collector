import React from 'react'
import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav'
import { Link } from 'react-router-dom'

const CustomNavbar = props => (
  <Navbar bg="dark" variant="dark" sticky="top">
    <Navbar.Brand>Collector</Navbar.Brand>
    <Nav className="mr-auto">
      <Nav.Link to="/" as={Link}>Home</Nav.Link>
      <Nav.Link to="/collection" as={Link}>Collection</Nav.Link>
    </Nav>
    <Navbar.Collapse className="justify-content-end">
      <Navbar.Text>
        User: <Link to="/me">Jhonemarkus</Link>
      </Navbar.Text>
    </Navbar.Collapse>
  </Navbar>
)

export default CustomNavbar