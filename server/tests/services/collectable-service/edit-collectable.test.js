import { describe, it, before, after,  } from 'mocha'
import { expect, assert } from 'chai'
import MongoUnit from 'mongo-unit'
import testData from '../../test-data'
import { InvalidParamsError } from '../../../src/util/errors'
import { editCollectable } from '../../../src/services/collectable-service'

describe('CollectableService.editCollectable', () => {
  beforeEach(() => MongoUnit.initDb(process.env.MONGO_URL, testData))
  afterEach(() => MongoUnit.drop())

  it('creates a new collectable', async () => {
    const aventador = await editCollectable({
      name: 'Aventador',
      parent: '5cdae0cb000a242ff80ff9e2',
      fields: [{
        name: 'Color',
        value: 'Black'
      }, {
        name: 'Price',
        value: 1000000
      }]
    })
    expect(aventador).not.to.be.null
    expect(aventador.id).not.to.be.null
  })

  it('edit an existing collectable', async () => {
    const car = {
      id: '5d0112113d60d7d2653c5c94',
      parent: '5cdae0cb000a242ff80ff9e1',
      name: '370Z',
      fields: [{
        name: 'Color',
        value: 'Red'
      }]
    }
    const updatedCar = await editCollectable(car)
    expect(updatedCar).not.to.be.null
    expect(updatedCar.name).to.equal(car.name)
    expect(updatedCar.id).to.equal(car.id)
    expect(updatedCar.fields[0].value).to.equal(car.fields[0].value)
  })

  it('fail when id not found', async() => {
    try {
      await editCollectable({id: 'non-existing'})
      assert.fail()
    } catch (err) {
      expect(err).to.be.an.instanceOf(InvalidParamsError)
    }
  })

  it('fail when not all required fields provided', async () => {
    try {
      await editCollectable({id: '5d0112113d60d7d2653c5c94'})
      assert.fail()
    }catch (e) {
      expect(e).to.be.an.instanceOf(InvalidParamsError)
    }
  })
})
