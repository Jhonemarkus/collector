import React from 'react'
import fetchMock from 'fetch-mock'
import { Route } from 'react-router-dom'
import { waitForDomChange, cleanup, wait } from 'react-testing-library'
import { fullRender } from '../TestUtils'
import actions, {getCollectionSuccess} from '../../store/actions'
import EditCollectionPage from '../../pages/edit-collection.page'

describe('Page EditCollection', () => {
  afterEach(() => {
    fetchMock.restore()
    cleanup()
  })

  test('render loading on start', () => {
    fetchMock.mock('*', { data: { getCollection: null } })
    const {getByText} = fullRender(<Route path="/collection/:id" component={EditCollectionPage} />, {route: '/collection/someId'})
    getByText('Loading...')
  })

  test('redirect to list if no collection found', async () => {
    fetchMock.mock('*', { data: { getCollection: null } })
    const { history, store, container } = fullRender(<Route path="/collection/:id" component={EditCollectionPage} />, { route: '/collection/non-existing' })
    expect(store.dispatch).lastCalledWith({id: "non-existing", type: actions.GET_COLLECTION})
    await fetchMock.flush(true)
    expect(history.location.pathname).toEqual('/collection')
  })

  test('render form if route is ADD', () => {
    const {getByText} = fullRender(<Route path="/collection/add" component={EditCollectionPage} />, {route: '/collection/add'})
    getByText('New Collection')
  })

  test('render form if route is EDIT', async () => {
    const collection = { id: 'existingId', name: 'Edit collection', userFields: []}
    fetchMock.mock('*', { data: { getCollection: collection } })
    const { getAllByLabelText } = fullRender(<Route path="/collection/:id" component={EditCollectionPage} />, {route: '/collection/existingId'})
    await fetchMock.flush(true)
    const name = getAllByLabelText('Name')[0]
    expect(name.value).toBe(collection.name)
  })
})