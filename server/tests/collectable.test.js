import server from '../src/index'
import request from 'supertest'
import { expect } from 'chai'
import testData from './test-data'
import MongoUnit from 'mongo-unit'
import config from 'config'

describe('GraphQL Collectable', () => {
  beforeEach(() => MongoUnit.initDb(process.env.MONGO_URL, testData))
  afterEach(() => MongoUnit.drop())

  it('get collectable by id', async () => {
    const id = '5d0112113d60d7d2653c5c94'
    const response = await request(server).post('/graphql')
      .send({
        query: `{ getCollectable(id: "${id}"){
          id name fields {name value} 
        } }`
      })
      .expect(200)
      .expect('Content-Type', /json/)
    expect(response.body.data.getCollectable).to.be.an.instanceof(Object)
    const collectable = response.body.data.getCollectable
    expect(collectable.id).to.equal(id)
    expect(collectable.name).to.equal('350Z')
    expect(collectable.fields[0].name).to.equal('Color')
    expect(collectable.fields[0].value).to.equal('Gray')
  })

  it('edit collectable', async () => {
    const car = {
      id: '5d0112113d60d7d2653c5c94',
      name: 'Impreza',
      fields: {
        name: 'Color',
        value: 'Blue'
      }
    }
    const response = await request(server).post('/graphql')
      .send({
        query: `
        mutation editCollectable($collectable: CollectableInput!){
          editCollectable(collectable: $collectable) {
            id name fields {name value}
          }
        }`,
        variables: {
          collectable: car
        }
      })
      .expect(200)
      .expect('Content-Type', /json/)
    expect(response.body.data.editCollectable).to.be.an.instanceof(Object)
    const collectable = response.body.data.editCollectable
    expect(collectable.id).to.equal(car.id)
    expect(collectable.name).to.equal('Impreza')
    expect(collectable.fields[0].name).to.equal('Color')
    expect(collectable.fields[0].value).to.equal('Blue')
  })

  it('add collectable', async () => {
    const car = {
      name: 'Aventador',
      parent: '5cdae0cb000a242ff80ff9e1',
      fields: {
        name: 'Color',
        value: 'Black'
      }
    }
    const response = await request(server).post('/graphql')
      .send({
        query: `
        mutation editCollectable($collectable: CollectableInput!){
          editCollectable(collectable: $collectable) {
            id name fields {name value}
          }
        }`,
        variables: {
          collectable: car
        }
      })
      .expect(200)
      .expect('Content-Type', /json/)
    expect(response.body.data.editCollectable).to.be.an.instanceof(Object)
    const collectable = response.body.data.editCollectable
    expect(collectable.id).not.to.be.null
    expect(collectable.name).to.equal('Aventador')
    expect(collectable.fields[0].name).to.equal('Color')
    expect(collectable.fields[0].value).to.equal('Black')
  })
})
