import React from 'react'
import { render } from 'react-testing-library'
import ErrorDisplay from '../../components/error-display'

describe('<ErrorDisplay />', () => {
  test('hide when no error', () => {
    const { queryByTestId } = render(<ErrorDisplay error={null} />)
    expect(queryByTestId('error-display')).toBeNull()
  })

  test('show error msg in red', () => {
    const { queryByTestId } = render(<ErrorDisplay error={'Something went wrong'} />)
    const element = queryByTestId('error-display')
    expect(element.className).toContain('alert alert-danger')
  })
})
