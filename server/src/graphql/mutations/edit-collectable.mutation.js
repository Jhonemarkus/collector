import { GraphQLNonNull } from 'graphql'
import { editCollectable } from '../../services/collectable-service'
import Collectable from '../types/collectable.type'
import CollectableInput from '../types/collectable-input.type'

export default {
  type: new GraphQLNonNull(Collectable),
  args: {
    collectable: { type: new GraphQLNonNull(CollectableInput) }
  }, 
  resolve: (source, {collectable}) => editCollectable(collectable)
}
