import React, {Fragment} from 'react'
import Alert from 'react-bootstrap/Alert'

export default props => {
  if (props.error) {
    return <Alert data-testid="error-display" variant="danger" dismissible>{props.error}</Alert>
  }
  return <Fragment></Fragment>
}
