export const actions = {
  SAVE_COLLECTION: 'SAVE_COLLECTION',
  SAVE_COLLECTION_SUCCESS: 'SAVE_COLLECTION_SUCCESS',
  DONE_REDIRECT: 'DONE_REDIRECT',
  LIST_COLLECTION: 'LIST_COLLECTION',
  LIST_COLLECTION_SUCCESS: 'LIST_COLLECTION_SUCCESS',
  GET_COLLECTION: 'GET_COLLECTION',
  GET_COLLECTION_SUCCESS: 'GET_COLLECTION_SUCCESS',
  SET_ERROR: 'SET_ERROR',
}

export const setError = (storePath, error) => {
  return {
    type: actions.SET_ERROR,
    error,
    storePath
  }
}

export const getCollectionSuccess = (collection) => {
  return {
    type: actions.GET_COLLECTION_SUCCESS,
    collection,
    redirect: collection == null
  }
}

export const getCollection = (id) => {
  return {
    type: actions.GET_COLLECTION,
    id
  }
}

export const listCollectionSuccess = ({list, count}, page) => {
  return {
    type: actions.LIST_COLLECTION_SUCCESS,
    list,
    count,
    page
  }
}

export const listCollection = (page) => {
  return {
    type: actions.LIST_COLLECTION,
    page
  }
}

export const saveCollection = (collection) => {
  return {
    type: actions.SAVE_COLLECTION,
    collection
  }
}

export const saveCollectionSuccess = (collection) => {
  return {
    type: actions.SAVE_COLLECTION_SUCCESS,
    collection
  }
}

export const doneRedirect = () => {
  return {
    type: actions.DONE_REDIRECT,
  }
}

export default actions
