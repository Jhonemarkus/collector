import { GraphQLInputObjectType, GraphQLID, GraphQLString, GraphQLNonNull, GraphQLList } from 'graphql'
import FieldInput from './collectable-field-input.type'

const CollectableInput = new GraphQLInputObjectType({
  name: 'CollectableInput',
  fields: {
    id: { type: GraphQLID },
    parent: { type: GraphQLID },
    name: { type: new GraphQLNonNull(GraphQLString) },
    fields: { type: new GraphQLList(new GraphQLNonNull(FieldInput)) },
  }
})

export default CollectableInput
