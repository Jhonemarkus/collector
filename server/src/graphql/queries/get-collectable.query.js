import { GraphQLID } from 'graphql'
import Collectable from '../types/collectable.type'
import { getCollectable } from '../../services/collectable-service'

export default {
  type: Collectable,
  args: {
    id: { type: GraphQLID}
  },
  resolve: async (source, {id}) => {
    const collectable = await getCollectable(id)
    return collectable
  }
}
