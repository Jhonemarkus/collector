import { GraphQLObjectType, GraphQLString, GraphQLList, GraphQLID, GraphQLNonNull } from 'graphql'
import Field from './field.type'

const Collection = new GraphQLObjectType({
  name: 'Collection',
  fields: {
    id: { type: GraphQLID },
    name: { type: new GraphQLNonNull(GraphQLString) },
    collectableName: { type: new GraphQLNonNull(GraphQLString) },
    userFields: { type: new GraphQLList(new GraphQLNonNull(Field)) }
  }
})

export default Collection
