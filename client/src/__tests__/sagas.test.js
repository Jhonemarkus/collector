import { expectSaga } from 'redux-saga-test-plan'
import fetchMock from 'fetch-mock'
import { saveCollection, listCollection, getCollection } from '../store/sagas'
import actions from '../store/actions'


describe('Sagas', () => {
  afterEach(() => {
    fetchMock.restore()
  })

  test('getCollection', () => {
    const id = 'abc123'
    const mockResponse = { data: { getCollection: { id, name: 'test collection', collectableName: 'colletable' } } }
    fetchMock.mock('*', mockResponse)
    return expectSaga(getCollection, {
      type: actions.GET_COLLECTION,
      id
    })
      .dispatch({
        type: actions.GET_COLLECTION_SUCCESS,
        collection: mockResponse.data.getCollection
      })
      .run()
  })
  
  test('saveCollection', () => {
    const mockResponse = {data: {editCollection: {name: 'Mock'}}}
    fetchMock.mock('*', mockResponse)
    return expectSaga(saveCollection, {
      type: actions.SAVE_COLLECTION,
      collection: {
        name: 'Test Collection',
        collectableName: 'Test',
        userFields: [{
          name: 'Name',
          type: 'STRING',
          userFilled: false
        }]
      }
    })
      .dispatch({ type: actions.SAVE_COLLECTION_SUCCESS, collection: mockResponse.data.editCollection })
      .run()
  })

  test('listCollection', () => {
    const mockList = [{id: '123', name: 'mock collection', collectableName: 'mock collectable', userFields: [{ name: 'Name', type: 'STRING', userFilled: false}]}]
    fetchMock.mock('*', {data: {listCollection: { count: 1, list: mockList }}})
    return expectSaga(listCollection, {
      type: actions.LIST_COLLECTION,
      page: 1
    })
      .dispatch({
        type: actions.LIST_COLLECTION_SUCCESS,
        list: mockList,
        count: 1
      })
      .run()
  })
})
