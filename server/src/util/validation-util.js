export const isNullOrEmptyString = (value) => {
  if (value != null){
    if (`${value}`.trim().length > 0) {
      return false
    }
  }
  return true
}

export const isNonEmptyArray = (value) => {
  if (Array.isArray(value)) {
    return value.length > 0
  }
  return false
}
