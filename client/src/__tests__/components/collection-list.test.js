import React from 'react'
import { cleanup, fireEvent, wait } from 'react-testing-library'
import { fullRender } from '../TestUtils'
import CollectionList from '../../components/collection-list'
import actions from '../../store/actions';

describe('CollectionList', () => {
  afterEach(cleanup)

  test('should have "new" button', () => {
    const { getByText } = fullRender(<CollectionList />)
    const newBtn = getByText(/new/i)
    expect(newBtn).toBeInstanceOf(HTMLButtonElement)
  })

  test('should have title and paginator', () => {
    const { getByText, getByTestId } = fullRender(<CollectionList />)
    getByText(/collection list/i)
    getByTestId('paginator')
  })

  test('table headers', () => {
    const { getByText } = fullRender(<CollectionList />)
    getByText(/name/i)
    getByText(/collectable/i)
    getByText('#Fields')
    getByText(/actions/i)
  })

  test('should fetch collections on mount', () => {
    const {store} = fullRender(<CollectionList />)
    const dispatch = store.dispatch
    expect(store.getState().collection).toEqual(expect.objectContaining({loading: true}))
    expect(dispatch).toHaveBeenCalledWith({type: actions.LIST_COLLECTION, page: 1})
  })

  test('should display all collections on store', () => {
    const list = [{
      id: '1',
      name: 'GT Sport',
      userFields: []
    }, {
      id: '2',
      name: 'GTAV',
      userFields: []
    }]
    const { getByText } = fullRender(<CollectionList />, {initialState: { collection: { list }}})
    getByText(list[0].name)
    getByText(list[1].name)
  })

})