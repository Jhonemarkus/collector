import { Schema, model } from 'mongoose'

const CollectableSchema = new Schema({
  parent: {
    type: Schema.Types.ObjectId,
    ref: 'Collection',
    required: true
  },
  name: {
    type: Schema.Types.String,
    required: true
  },
  fields: [{
    name: {
      type: Schema.Types.String,
      require: true
    },
    value: {
      type: Schema.Types.Mixed,
      required: true
    }
  }],
})

const CollectableModel =  model('Collectable', CollectableSchema, 'collectable')
export default CollectableModel
