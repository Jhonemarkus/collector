import { describe, it } from 'mocha'
import { expect } from 'chai'
import request from 'supertest';
import server from '../src/index';

async function checkType(name, kind) {
  const {body: {data}} = await request(server).post('/graphql')
    .send({ query: `{ __type(name: "${name}") { name kind } }` })
    .expect(200)
    .expect('Content-Type', /json/)
  expect(data).to.be.an.instanceof(Object)
  expect(data.__type).not.to.be.null
  expect(data.__type.name).to.equal(name)
  expect(data.__type.kind).to.equal(kind)
}
  
async function checkMutation(name) {
  const {body: {data}} = await request(server).post('/graphql')
  .send({ query: `{ __type(name: "Mutation") { name kind fields {name}  } }` })
  .expect(200)
  .expect('Content-Type', /json/)
  expect(data).to.be.an.instanceof(Object)
  expect(data.__type).not.to.be.null
  expect(data.__type.name).to.equal('Mutation')
  expect(data.__type.fields).to.deep.include({name})
}

async function checkQuery(name) {
  const {body: {data}} = await request(server).post('/graphql')
  .send({ query: `{ __type(name: "Query") { name kind fields {name}  } }` })
  .expect(200)
  .expect('Content-Type', /json/)
  expect(data).to.be.an.instanceof(Object)
  expect(data.__type).not.to.be.null
  expect(data.__type.name).to.equal('Query')
  expect(data.__type.fields).to.deep.include({name})
}

describe('GraphQL', () => {
  it('has "CollectionOutput" type', async () => {
    await checkType('CollectionOutput', 'OBJECT')
  })

  it('has "FieldType" enum', async () => {
    await checkType('FieldType', 'ENUM')
  })

  it('has "Field" type', async () => {
    await checkType('Field', 'OBJECT')
  })

  it('has "Collection" type', async () => {
    await checkType('Collection', 'OBJECT')
  })

  it('has "CollectionInput" type', async () => {
    await checkType('CollectionInput', 'INPUT_OBJECT')
  })
  it('has "FieldInput" type', async () => {
    await checkType('FieldInput', 'INPUT_OBJECT')
  })

  it('has "editCollection" mutation', async () => {
    await checkMutation('editCollection')
  })

  it('has "listCollection" query', async() => {
    await checkQuery('listCollection')
  })

  it('has "getCollection" query', async() => {
    await checkQuery('getCollection')
  })

  it('has "CollectableField" type', async() => {
    await checkType('CollectableField', 'OBJECT')
  })

  it('has "Collectable" type', async() => {
    await checkType('Collectable', 'OBJECT')
  })

  it('has "CollectableFieldInput" type', async() =>{
    await checkType('CollectableFieldInput', 'INPUT_OBJECT')
  })

  it('has "CollectableInput" type', async() =>{
    await checkType('CollectableInput', 'INPUT_OBJECT')
  })
})
