import { GraphQLNonNull } from 'graphql'
import { editCollection } from '../../services/collection-service'
import Collection from '../types/collection.type'
import CollectionInput from '../types/collection-input.type'

export default {
  type: new GraphQLNonNull(Collection),
  args: {
    collection: { type: new GraphQLNonNull(CollectionInput) }
  }, 
  resolve: (source, {collection}) => editCollection(collection)
}
