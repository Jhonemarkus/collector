import { Schema, model } from 'mongoose'

const CollectionSchema = new Schema({
  name: {
    type: String,
    required: true,
    unique: true
  },
  collectableName: {
    type: String,
    required: true
  },
  userFields: [{
    name: {
      type: String,
      required: true,
    },
    userFilled: {
      type: Boolean,
      required: true,
      default: false
    },
    type: {
      type: String,
      required: true
    },
    valueList: {
      type: [String]
    }
  }],
})

const CollectionModel =  model('Collection', CollectionSchema, 'collection')
export default CollectionModel
