import { describe, it } from 'mocha'
import { expect } from 'chai'
import Collection from '../../src/models/collection'

describe('Models - Collection', () => {
  it('should exist', () => {
    expect(Collection.modelName).to.equal('Collection')
  })
})
