import { GraphQLInputObjectType, GraphQLNonNull, GraphQLString, GraphQLBoolean, GraphQLList } from 'graphql'

const FieldInput = new GraphQLInputObjectType({
  name: 'CollectableFieldInput',
  fields: {
    name: { type: new GraphQLNonNull(GraphQLString)},
    value: { type: new GraphQLNonNull(GraphQLString)}
  }
})

export default FieldInput
