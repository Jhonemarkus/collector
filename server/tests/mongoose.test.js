import { describe, it } from 'mocha'
import { expect } from 'chai'
import setupConnection from '../src/mongoose'
import mongoose from 'mongoose'

describe('Mongoose connection', () => {
  it('should connect to MongoDB', () => {
    setupConnection().then(() => {
      expect(mongoose.connection.readyState).to.equal(1)
    })
  })
})
