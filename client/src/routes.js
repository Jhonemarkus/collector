import React from 'react'
import { BrowserRouter, Route, Switch } from 'react-router-dom'

import AppContainer from './AppContainer'
import EditCollectionPage from './pages/edit-collection.page'
import CollectionList from './components/collection-list'

export default function() {
  return (
    <BrowserRouter>
      <AppContainer>
        <Switch>
          <Route path='/collection/add' component={EditCollectionPage} />
          <Route path='/collection/:id' component={EditCollectionPage} />
          <Route path="/collection" component={CollectionList} />
        </Switch>
      </AppContainer>
    </BrowserRouter>
  )
}
