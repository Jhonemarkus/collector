import actions from './actions'

export default function reducer( state = {}, action) {
  switch (action.type) {
    case actions.SET_ERROR:
      return { ...state, [action.storePath]: { ...state[action.storePath], loading: false}, error: action.error }
    case actions.GET_COLLECTION_SUCCESS:
      return { ...state, collection: { ...state.collection, loading: false, current: action.collection, redirect: action.redirect } }
    case actions.GET_COLLECTION:
      return { ...state, collection: { ...state.collection, loading: true, current: null } }
    case actions.LIST_COLLECTION_SUCCESS:
      return { ...state, collection: { ...state.collection, loading: false, list: action.list, page: action.page, count: action.count } }
    case actions.LIST_COLLECTION:
      return { ...state, collection: { ...state.collection, loading: true } }
    case actions.DONE_REDIRECT:
      return { ...state, redirect: false }
    case actions.SAVE_COLLECTION:
      return { ...state, collection: { ...state.collection, loading: true } }
    case actions.SAVE_COLLECTION_SUCCESS:
      return { ...state, collection: { ...state.collection, loading: false }, redirect: true }
    default:
      return state
  }
}

