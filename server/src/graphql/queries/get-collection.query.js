import { GraphQLID } from 'graphql'
import Collection from '../types/collection.type'
import { getCollection } from '../../services/collection-service'

export default {
  type: Collection,
  args: {
    id: { type: GraphQLID}
  },
  resolve: async (source, {id}) => {
    const collection = await getCollection(id)
    return collection
  }
}
