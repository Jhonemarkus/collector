import React, {Component, Fragment} from 'react'
import { connect } from 'react-redux'
import {Link} from 'react-router-dom'
import Table from 'react-bootstrap/Table'
import Button from 'react-bootstrap/Button';
import { listCollection } from '../store/actions'
import CollectionRow from './collection-row'
import Paginator from './paginator'

class CollectionList extends Component {
  componentDidMount() {
    this.props.listCollection(1)
  }

  onChangePage = (page) => {
    const {total, page: current} = this.props
    if (page && page !== current && page >= 0 && page && page <= Math.ceil(total / 10) ) {
      this.props.listCollection(page)
    }
  }

  render(){
    const { list, page, total } = this.props
    return (
      <Fragment>
        <h2>
          Collection List
          <Link to="/collection/add" className="float-right">
            <Button variant="info">New</Button>
          </Link>
        </h2>
        <Table responsive>
          <thead>
            <tr>
              <th>Name</th>
              <th>Collectable</th>
              <th>#Fields</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
            {list && list.map((collection) => 
            <CollectionRow key={collection.id} collection={collection} />
            )}
          </tbody>
        </Table>
        <Paginator page={page} total={total} onChange={this.onChangePage}/>
      </Fragment>
    )
  }
}

const state2props = state => ({
  list: state.collection.list,
  total: state.collection.count,
  page: state.collection.page
})

export default connect(state2props, {listCollection})(CollectionList)
