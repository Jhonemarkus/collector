import React from 'react'
import { Provider } from 'react-redux'
import { render } from 'react-testing-library'
import { setupStore } from '../store/store'
import { Router } from 'react-router-dom'
import { createMemoryHistory } from 'history'

export const throwOnPropTypeError = () => {
  const originalFn = console.error
  jest.spyOn(console, 'error').mockImplementation((msg, ...args) => {
    if (/(Failed prop type)/.test(msg)) {
      throw new Error(msg)
    }
    originalFn(msg, ...args)
  })
}

export const renderWithRedux = (comp, {initialState, store = setupStore(initialState)} = {}) => {
  return {
    ...render(<Provider store={store}>{comp}</Provider>),
    store
  }
}

export const fullRender = (comp, {
  initialState,
  store = setupStore(initialState),
  route = '/',
  history = createMemoryHistory({initialEntries: [route]})
} = {}) => {
  jest.spyOn(store, 'dispatch')
  return {
    ...render(
      <Provider store={store}>
        <Router history={history}>
          {comp}
        </Router>
      </Provider>
    ),
    store,
    history,
  }
}

test('exists', () => {})
